#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h> 	//for madvise
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>

long lrand(){
	return ((long)rand()<<(sizeof(int) * 8) | rand());
}

void rnd_read(char *map, long long size){
	int seed = 23;
	srand(seed);
	char c;
	long long which;
	for (long long i = 0; i < size; i++){
		which = lrand() % size;
		c = map[which];
		map[which] = 'a';
	}
}


int main(int argc, char *argv[]){
	int fd;
	char *map;
	struct stat info;
	long long length;
	char *filename;

	if(argc < 2){
		printf("Specify a filename, yo\n");
		exit(-1);
	}

	filename = argv[1];
	printf("Triggering VM_DUMMY_READ via madvise...\n");

	fd = open(filename, O_RDWR);
	if ( fd == -1){
		printf("Error opening file\n");
		exit(-1);
	}
	if (fstat(fd, &info) == -1){
		printf("Error performing stat on the file\n");
		exit(-1);
	}
	length = (long long)info.st_size;
	printf("Filesize: %lld\n", length);

	map = mmap(NULL, length, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);

	if (map == MAP_FAILED){
		printf("Error mapping\n");
		exit(-1);
	}

	madvise(map, length, MADV_DUMMY); // 5);	
	rnd_read(map, length);

	munmap(map, length);
	close(fd);
	return(0);


}
