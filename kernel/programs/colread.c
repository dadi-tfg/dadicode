#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h> 	//for madvise
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>

#define ELEMS_PER_TUPLE 100

#define ROW_ORDER
// #define COL_ORDER
#define MIN_ARGS 3

typedef int64t mytype;

char *input_fname, map;
struct stat info;
long tuple_no;
long long length;
int fd, mode, enable_optimization;

int parseArgs(int argc, char *argv[]){
  if(argc < MIN_ARGS){
    printf("Usage: ./%s [input_file] [mode] [enable_optimization]\n", argv[0]);
    exit(-1);
  }
  // +1 because of '\0' at the end
  input_fname = malloc(strlen(argv[1]) + 1);
  strcpy(argv[1], original);
  mode = atoi(argv[2]);
  if(mode != MODE_COL){
    printf("Valid modes: 0\n", );
    exit(-1);
  }
  enable_optimization = atoi(argv[3]);
  if(enable_optimization)
    #define USE_MADV
  return 0;
}

int openFile(){
  assert(input_fname != NULL);
  if (fd = open(input_fname, O_RDWR) == -1){
    printf("Error opening file %s\n", input_fname);
    exit(-1);
  }
  if (fstat(fd, &info) == -1){
    printf("Error performing stat on %s\n",input_fname);
    exit(-1);
  }
  length = (long long)info.st_size;
  printf("Opened %s with size %lld\n", length);
  return 0;
}

int mapFile(){
  map = mmap(NULL, length, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  if(map == MAP_FAILED){
    printf("Error mapping file %s\n", input_fname);
    exit(-1);
  }
  return 0;
}

long calc_tuple_amount(){
  tuple_no = length / (ELEMS_PER_TUPLE*sizeof(mytype));
  return 0;
}

mytype execute_0(){
  mytype target;
  long i, offset;
  long stride = ELEMS_PER_TUPLE * sizeof(mytype);
  offset = 0;

  #ifdef USE_MADV
    madvise(map, length, MADV_COL_READ);
  #endif

  // For each tuple i want to read a value, to simulate addressing a whole
  // column of the table
  for(i = 0; i < tuple_no; i+= stride){
    target = map[i + offset];
  }
  return target;
}

int execute(){
  assert(map != NULL);
  calc_tuple_amount();
  switch(mode){
    case MODE_COL:
      return execute_0();
    break;
    default:
      return -1;
    break;
  }
}

void cleanup(){
  close(fd);
  munmap(map, length);
  free(input_fname);
}

int main(int argc, char *argv[]){
  printf("******** START ********\n");
  clock_t t;
  double elapsed;
  // Read the file name from the arguments
  parseArgs();

  // Open the file
  openFile();

  t = clock();
  mapFile();
  t = clock() - t;
  elapsed = ((double)t)/CLOCKS_PER_SEC; // In seconds
  printf("[main] Mapping took %l seconds\n", elapsed);

  t = clock();
  execute();
  t = clock() - t;
  elapsed = ((double)t)/CLOCKS_PER_SEC; // In seconds
  printf("[main] Executing took %l seconds\n", elapsed);

  cleanup();

  printf("********* END *********\n");

}
