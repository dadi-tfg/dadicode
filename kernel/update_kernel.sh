#!/bin/bash
#"void" | su

FOLDER=/home/void/linux-5.5.2

# Check privilege
if [ "$(whoami)" != "root" ]; then
	echo "Amiga no sos root bye"
	exit 101
fi

# Check build folder
if [[ ! -d "$FOLDER" ]]; then
	echo "Noesiste lacarpeta $FOLDER"
	exit 102
fi

OLD="$(pwd)"

# Colocarnos en la carpeta
cd "$FOLDER"

# Copiar el mapa
cp -v System.map /boot/System.map-5.5.2_m
cp -v arch/x86_64/boot/bzImage /boot/vmlinuz-5.5.2_m
cp -v .config /boot/config-5.5.2_m

# Dracuteo
dracut -f initramfs-5.5.2_m.img 5.5.2_m
cp -v initramfs-5.5.2_m.img /boot/
update-grub

cd "$OLD"
