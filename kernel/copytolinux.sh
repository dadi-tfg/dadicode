#!/bin/bash
USERPATH=/home/void
LINFOLDER=linux-5.5.2
PREFIX=${USERPATH}/${LINFOLDER}
MOD=${USERPATH}/dadicode/kernel

MMP=include/linux/mm.h
MMANP=include/uapi/asm-generic/mman-common.h
# MMAN in tools is just mman but prepending /tools/

MM=${PREFIX}/${MMP}
MMAN=${PREFIX}/${MMANP}
MMANT=${PREFIX}/tools/${MMANP}

# Check if the user has root privilege
if [ "$(whoami)" != "root" ]; then
	echo "Script must be run as user: root"
	exit 101
fi

# Check if the Linux source files exist
if [[ ! -f "$MM" ]]; then
	echo "File $MM does not exist"
	exit 102
elif [[ ! -f "$MMAN" ]]; then
	echo "File $MMAN does not exist"
	exit 103
elif [[ ! -f "$MMANT" ]]; then
	echo "File $MMANT does not exist"
	exit 104
fi

# Check if the modified files exist
if [[ ! -f "$MOD/mm.h" ]]; then
	echo "There is no $MOD/mm.h"
	exit 105
elif [[ ! -f "$MOD/mman-common-inc.h" ]]; then
	echo "There is no $MOD/mman-common-inc.h"
	exit 107
elif [[ ! -f "$MOD/mman-common-tools.h" ]]; then
	echo "There is no $MOD/mman-common-tools.h"
	exit 106
fi


#echo ${MM}
#echo ${MMAN}
#echo ${MMANT}
echo "Linux source folder is at $PREFIX"
echo "Mod folder is at $MOD"

# 1 > Backup and copy the mm.h file
cp ${MM} ${MM}.bak
cp -v ${MOD}/mm.h ${MM}

# 2 > Backup and copy the mman-common.h file in the normal place
cp ${MMAN} ${MMAN}.bak
cp -v ${MOD}/mman-common-inc.h ${MMAN}

# 3 > Backup and copy the mman-common.h file in the include folder
cp ${MMANT} ${MMANT}.bak
cp -v ${MOD}/mman-common-tools.h ${MMANT}

# Copy the mm header containing the flag/s
#cp -v ${USERPATH}/linux-5.5.2/include/linux/mm.h ${USERPATH}/linux-5.5.2/include/linux/mm.h.bak
#cp -v ${USERPATH}/dadicode/mm.h ${USERPATH}/linux-5.5.2/include/linux/mm.h


#cp -v ${USERPATH}/linux-5.5.2/include/uapi/asm-generic/mman-common.h ${USERPATH}/linux-5.5.2/include/uapi/asm-generic/mman-common.h.bak
#cp -v ${USERPATH}/dadicode/mman-common-inc.h ${USERPATH}/linux-5.5.2/include/uapi/asm-generic/mman-common.h

#cp -v ${USERPATH}/linux-5.5.2/tools/include/uapi/asm-generic/mman-common.h ${USERPATH}/linux-5.5.2/tools/include/uapi/asm-generic/mman-common.h.bak
#cp -v ${USERPATH}/dadicode/mman-common-tools.h ${USERPATH}/linux-5.5.2/tools/include/uapi/asm-generic/mman-common.h 

#cp -v ${USERPATH}/linux-5.5.2/mm/madvise.c ${USERPATH}/linux-5.5.2/mm/madvise.c.bak
#cp -v ${USERPATH}/dadicode/madvise.c ${USERPATH}/linux-5.5.2/mm/madvise.c

#cp -v ${USERPATH}/linux-5.5.2/mm/filemap.c ${USERPATH}/linux-5.5.2/mm/filemap.c.bak
#cp -v ${USERPATH}/dadicode/filemap.c ${USERPATH}/linux-5.5.2/mm/filemap.c
