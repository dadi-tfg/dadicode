#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/time.h>
#include "operaciones.h"

void error(char * hutsegite_puntua)
{
	perror( hutsegite_puntua);
	exit(-1);
}

int main(int argc, char * argv[])
{
// int getrusage(int who, struct rusage *usage);
// int who:
// 	RUSAGE_SELF suma de todos los threads del proceso
// 	RUSAGE_CHILDREN suma de los hijos muertos y esperados
// 	RUSAGE_THREAD

// void *mmap (void *addr, size_t length, int prot, int flags, int fd, off_t offset);
// int munmap(void *addr, size_t length); 

	if (argc != 3){
		printf("Usage: %s [METHOD] [MODE]\nMETHODS:\n\t0 MMAP\n\t1 I/O Stack\nMODES:\n\t0 READ\n\t1 WRITE\n", argv[0]);
		return(-1);
	}
	
	printf("DTBS - DADI Toolbox Bercion Sero\n");
	struct rusage * uso;
	int metodo = atoi(argv[1]);
	int modo   = atoi(argv[2]);
	
	if(metodo < MIN_METHOD || metodo > MAX_METHOD)
	{
		printf("Main: bad method\n");
		exit(-1);
	}
	
	switch(metodo)
	{
		case 0: // Secuencial MMAP
			mmap_secuencial(modo);
		break;

		case 1: //Aleatorio MMAP
			mmap_aleatoria(modo);
		break;
	
	}

	if (!getrusage(RUSAGE_SELF, uso)){
		error("Getrusage");
	}
	// AQUI PRINTEAR MIERDA DEL USAGE

}
