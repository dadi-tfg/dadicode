#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <string.h>


#define MIN_ARGS 3
#define MAX_ARGS 3

static int page_size;

long lrand()
{
	return ( (long)rand() << (sizeof(int) * 8) | rand());
}

void sequential(char * map, long long size)
{
	char c;
	int ventitre = 23;
	for ( long long i = 0; i < size/2; i++)
	{
//		fprintf(stdout, "%lld", i);
		c = map[i];
	//	memcpy (&map[i], &ventitre , sizeof(int));
		//map[i] = 23;
	}
}

void sequential_cont(char * map, long long size)
{
	struct rusage u;
	getrusage(RUSAGE_SELF, &u);
	int faults = u.ru_minflt;
	int inblocks = 0;
	char c;
	int cont = 0;
	fprintf(stdout, "Faults iniciales%d\n", faults );
	for (long long i = 0; i < size; i++)
	{
		c = map[i];
		getrusage(RUSAGE_SELF, &u);
		if(u.ru_minflt > faults)
		//if(u.ru_inblock > inblocks)
		{
			fprintf(stdout, "INBLOCKS %d \t",u.ru_inblock );
			//fprintf(stdout, "Dir %x \tIndice %d \tIn Blocks %ld\n", &map[i], i, u.ru_inblock);
			fprintf(stdout, "Direccion %d\tDiferencia %d\tFaults %d\n", i, i-cont, u.ru_minflt);
			faults = u.ru_minflt;
			//inblocks = u.ru_inblock;
			cont = i;
		}
	}
}

void random_a( char * map, long long size )
{
	time_t t;
	srand((unsigned) time(&t));
	char c;
	long long cual;
	for (long long i = 0; i < size; i++)
	{
		cual = lrand() % size;
//		fprintf(stdout,"-[%d]-",cual);
		c = map[cual];
		//map[cual] = 23;
	}
		//c = map[rand() % size];

}

/**
Ver cada cuanto hay un minor
**/

// Que haga siempre major fault
void random_b( char * map, long long size )
{
	int blocksize;
	time_t t;
	srand((unsigned) time(&t));
	char c;
	long long cual;
	for (long long i = 0; i < size; i++)
	{
		cual = lrand() % blocksize;
//		fprintf(stdout,"-[%d]-",cual);
		c = map[cual];
	}
		//c = map[rand() % size];

}




int lectura(char * filename, char * tipo_lec)
{
	struct rusage u;
	//fprintf(stdout, "|\tinicio lectura\n");
	char *map;
	int fd;
	struct stat info;
	off_t offset, pa_offset;
	long long tam;
//	size_t length;
	long long length;
	ssize_t s;



	// Abrir fichero y determinar size
	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		fprintf(stderr, "|\t|\tError opening file\n");
		return(-1);
	}
	if (fstat(fd, &info) == -1)
	{
		fprintf(stderr, "|\t|\tError performing stat on the file to determine its size\n");
		return(-1);
	}
	length = (long long)info.st_size;
	//fprintf(stdout, "|\t|\tThe file %s reports a %lld size\n", filename, length);

	getrusage(RUSAGE_SELF, &u);
	fprintf(stdout, "PRE FAULTS %d \n",u.ru_minflt );
	fprintf(stdout, "PRE INBLOCKS %d \n",u.ru_inblock );

	//posix_fadvise(fd,0, info.st_size, POSIX_FADV_DONTNEED);
	map = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if (map == MAP_FAILED)
	{
		fprintf(stderr, "|\tError mapping file\n");
		return(-1);
	}


	if (tipo_lec == "seq")
	{
		sequential(map, length);
	}
	else if (tipo_lec == "ale"){
		random_a(map, length);
	}
	else if (tipo_lec == "seq_cont"){
		sequential_cont(map, length);
	}
	getrusage(RUSAGE_SELF, &u);
	fprintf(stdout, "POST FAULTS %d \n",u.ru_minflt );
	fprintf(stdout, "POST INBLOCKS %d \n",u.ru_inblock );

	// CLEANUP TASKS
	//fprintf(stdout, "|\tfin lectura\n");
	munmap(map, length);
	close(fd);
	// BYE
	return(0);
}

void main(int argc, char * argv[])
{
	struct rusage uso;

//	fprintf(stdout, "Inicio programa de pruebas de mmap\n");

	if (argc < MIN_ARGS || argc > MAX_ARGS)
	{
		fprintf(stderr, "USO: %s mode file\n\nAvailable modes:\n0\tSEQ\n1\tRND\n", argv[0]);
		exit(-1);
	}

	//page_size = getpagesize();
	//fprintf(stdout,"Page size is %d\n", page_size);
	int res;
	if (argv[1] == "SEQ" || argv[1] == "seq" || atoi(argv[1]) == 0)
		res = lectura(argv[2], "seq");
	else if (argv[1] == "RND" || argv[1] == "rnd" || atoi(argv[1]) == 1)
		res = lectura(argv[2], "ale");
	else if (argv[1] == "SEQ_CONT" || argv[1] == "seq_cont" || atoi(argv[1]) == 2)
		res = lectura(argv[2], "seq_cont");

	if (res == -1)
	{
		fprintf(stdout,"Error running the requested mode\n");
	}
	// void *mmap(void *addr, size_t length, int prot, int flags,
	// 		int fd, off_t offset);
	// void munmap(void * addr, size_t length);

	//fprintf(stdout, "Fin programa de pruebas de mmap\n");
	//int aber = -1;
	int aber = getrusage(RUSAGE_SELF, &uso);
	fprintf(stdout, "Stats\n MIN %ld MAJ %ld\n SWP %ld INB %ld OUTB %ld CTX %ld\n",uso.ru_minflt, uso.ru_majflt, uso.ru_nswap, uso.ru_inblock, uso.ru_oublock, uso.ru_nvcsw + uso.ru_nivcsw);
}
