#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

static int page_size;
static int block_size;

long lrand()
{
	return ( (long)rand() << (sizeof(int) * 8) | rand());
}

void rnd(int verbose, int block_aligned_rnd, char * map, long long length )
{
	srand(232323);
	char c;
	long long cual;
	int cont = 0;
	int faults = 0;
	struct rusage u;

	for (long long i = 0; i < length; i++)
	{
		cual = block_aligned_rnd? (lrand() % page_size) : lrand();
		cual = cual % length;
		c = map[cual];
		if(verbose){
			getrusage(RUSAGE_SELF, &u);
			if(u.ru_minflt > faults){
				fprintf(stdout, " minFAULT! %d ", i - cont);
				faults = u.ru_minflt;
				cont = i;
			}
		}
	}
	fprintf(stdout, "\n");

}


int lectura(int fadvise, int verbose, char * filename){
	char *map, c;
	struct rusage u;
	int fd;
	struct stat info;
	off_t offset, pa_offset;
	long long length;
	int f_init, f_end;

	// Abrir fichero y determinar size
	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		fprintf(stderr, "rnd_fadvise: Error opening file\n");
		return(-1);
	}
	if (fstat(fd, &info) == -1)
	{
		fprintf(stderr, "seq_fadvise: Error stat-ing on file\n");
		return(-1);
	}
	length = (long long)info.st_size;

// PRINT PRE
if (fadvise)
	fprintf(stdout, "seq_fadvise: %s\n", filename);
else
	fprintf(stdout, "seq_no_fadvise: %s\n", filename);

// MAP
	getrusage(RUSAGE_SELF, &u);
	f_init = u.ru_minflt;
	if(fadvise)
		posix_fadvise(fd,0, info.st_size, POSIX_FADV_DONTNEED);
	map = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

// LECTURA
	int block_aligned_rnd = 0;
	rnd(verbose, block_aligned_rnd, map, length);


// PRINT POST
	getrusage(RUSAGE_SELF, &u);
	f_end = u.ru_minflt;
	int pages = u.ru_inblock*512/1024;
	fprintf(stdout, "DELTAmfault:%d\tINBLOCKS:%d\tPAGES:%ld\n", f_end - f_init, u.ru_inblock, pages);
	munmap(map, length);
	close(fd);
}


void main(int argc, char * argv[]) {
	struct rusage uso;
	int verbose;
	int fadvise;

	//fprintf(stdout, "Inicio seq_fadvise\n"); // COMPR. ARGS
	if (argc != 4)
	{
		fprintf(stderr, "USAGE: %s filename verbose fadvise\n", argv[0]);
		exit(-1);
	}

	verbose = atoi(argv[2]);
	fadvise = atoi(argv[3]);
	page_size = getpagesize();
	lectura(fadvise, verbose, argv[1]);

	getrusage(RUSAGE_SELF, &uso);
	//fprintf(stdout, "SEQ_FADVISE\n MIN %ld MAJ %ld\n SWP %ld INB %ld OUTB %ld CTX %ld\n",uso.ru_minflt, uso.ru_majflt, uso.ru_nswap, uso.ru_inblock, uso.ru_oublock, uso.ru_nvcsw + uso.ru_nivcsw);

	//fprintf(stdout, "Fin seq_fadvise\n");
}
