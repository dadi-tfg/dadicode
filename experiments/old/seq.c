#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

int lectura_seq(int fadvise, int verbose, char * filename){
	char *map, c;
	struct rusage u;
	int fd;
	struct stat info;
	off_t offset, pa_offset;
	long long length;
	int f_init, f_end;

	// Abrir fichero y determinar size
	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		fprintf(stderr, "seq: Error opening file\n");
		return(-1);
	}
	if (fstat(fd, &info) == -1)
	{
		fprintf(stderr, "seq: Error stat-ing on file\n");
		return(-1);
	}
	length = (long long)info.st_size;

// PRINT PRE
	if (fadvise)
		fprintf(stdout, "seq_fadvise: %s\n", filename);
	else
		fprintf(stdout, "seq_no_fadvise: %s\n", filename);
// MAP
	getrusage(RUSAGE_SELF, &u);
	f_init = u.ru_minflt;
	if(fadvise)
		posix_fadvise(fd,0, info.st_size, POSIX_FADV_DONTNEED);
	map = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

// LECTURA
	if (verbose)
	{
		int cont = 0;
		int faults = 0;
		for (long long i = 0; i < length; i++)
		{
			c = map[i];
			getrusage(RUSAGE_SELF, &u);
			if(u.ru_minflt > faults)
			//if(u.ru_inblock > inblocks)
			{
				fprintf(stdout, " minFAULT! %d ", i - cont);
				//fprintf(stdout, "Dir %x \tIndice %d \tIn Blocks %ld\n", &map[i], i, u.ru_inblock);
				//fprintf(stdout, "Direccion %d\tDiferencia %d\tFaults %d\n", i, i-cont, u.ru_minflt);
				faults = u.ru_minflt;
				//inblocks = u.ru_inblock;
				cont = i;
			}
		}
		fprintf(stdout, "\n");
	}else{
		for ( long long i = 0; i < length; i++)
			c = map[i];
	}

	// PRINT POST
	getrusage(RUSAGE_SELF, &u);
	f_end = u.ru_minflt;
	int pages = u.ru_inblock*512/1024;
	fprintf(stdout, "DELTAmfault:%d\tINBLOCKS:%d\tPAGES:%ld\n", f_end - f_init, u.ru_inblock, pages);
	munmap(map, length);
	close(fd);
}


void main(int argc, char * argv[]) {
	struct rusage uso;
	int verbose;
	int fadvise;

	//fprintf(stdout, "Inicio seq_fadvise\n"); // COMPR. ARGS
	if (argc != 4)
	{
		fprintf(stderr, "USAGE: %s filename verbose fadvise\n", argv[0]);
		exit(-1);
	}

	verbose = atoi(argv[2]);
	fadvise = atoi(argv[3]);
	lectura_seq(fadvise, verbose, argv[1]);

	getrusage(RUSAGE_SELF, &uso);
	//fprintf(stdout, "SEQ_FADVISE\n MIN %ld MAJ %ld\n SWP %ld INB %ld OUTB %ld CTX %ld\n",uso.ru_minflt, uso.ru_majflt, uso.ru_nswap, uso.ru_inblock, uso.ru_oublock, uso.ru_nvcsw + uso.ru_nivcsw);

	//fprintf(stdout, "Fin seq_fadvise\n");
}
