#!/bin/bash

# Function to clear caches when wanted
drop_caches() {
  (echo 3) > /proc/sys/vm/drop_caches;
}

if [ "$(whoami)" != "root" ]; then
        echo "Script must be run as user: root"
        exit 255
fi

# En mi instalacion arch
# ROOT=/home/mikeltxo/TFG/files/

# ROOT=/home/void/files/
# ROOT=/mnt/nvme/mikeltxo/TFG/files/
ROOT=./
# Get parameters from console args
THREADS=$1

# Default thread count
if [ -z ${THREADS} ]; then
  THREADS=1
fi

drop_caches

IN=${ROOT}"fich_256M"
if [[ ! -f "$IN" ]]; then
  echo "$IN does not exist, creating file"
  dd if=/dev/urandom of="$IN" bs=1M count=256
fi

IN=${ROOT}"fich_1G"
if [[ ! -f "$IN" ]]; then
  echo "$IN does not exist, creating file"
  dd if=/dev/urandom of="$IN" bs=1M count=1024
fi

IN=${ROOT}"fich_4G"
if [[ ! -f "$IN" ]]; then
  echo "$IN does not exist, creating file"
  dd if=/dev/urandom of="$IN" bs=1M count=4096
fi

IN=${ROOT}"fich_16G"
if [[ ! -f "$IN" ]]; then
  echo "$IN does not exist, creating file"
  dd if=/dev/urandom of="$IN" bs=1M count=16384
fi

for FILE in fich_256M fich_1G fich_4G fich_16G
do
  IN=${ROOT}${FILE}
  # Check if the file exists
  if [[ ! -f "$IN" ]]; then
    echo "$IN does not exist, exiting"
    exit 101
  fi

  echo "Processing file $IN"

  OUT=./tests_${FILE}.csv
  if [[ -f "$OUT" ]]; then
    echo "$OUT already exists, backing up file"
    echo "$OUT -> $OUT.old"
    mv "$OUT" "$OUT.old"
  fi
  touch ${OUT} # create the file to save the results
  echo "Iteration,Cache,Pattern,Test,Blocksize,Bandwidth(GBps),Time(s)" | tee -a >> ${OUT}
  for CACHE in cold warm
  do
    #echo ${CACHE} | tee -a ./tests_${FILE} # header for cache mode
    for MODE in seq rnd
    do
      #echo ${MODE} | tee -a ./tests_${FILE} # header for access pattern
      if [ "$MODE" = "seq" ]; then
        ACCESS=""
      else
        ACCESS="--randomaccess"
      fi
      for TEST in readmmap readsyscall writemmap writesyscall
      do
        #echo ${TEST} | tee -a ./tests_${FILE} # header for test type
        for BLOCK in 8192 #4096 8192 16384
        do
	  # COMMAND="./fa -b ${BLOCK} --${TEST} -f ${ROOT}${FILE} ${ACCESS} ${CREATE} ${SIZE} -t $t --silent"
          for ITER in {1..5}
	  do
		  echo -n "${ITER},${CACHE},${MODE},${TEST},${BLOCK}," | tee -a ${OUT}
		  echo "$(./fa -b ${BLOCK} --${TEST} -f ${ROOT}${FILE} ${ACCESS} -t ${THREADS} -x ${ITER} --silent)" | tee -a ${OUT}
          if [ "$CACHE" = "cold" ]; then
            drop_caches
          fi
        done
        done
        # echo ",,,,,,," | tee -a ${OUT} # Empty line in csv
        # SEPARATOR BETWEEN BLOQUESITOS
        # echo ",,,,,,," | tee -a ${OUT}
      done
      # SEPARATOR BETWEEN SEQ AND RANDOM
      # echo ",,,,,,," | tee -a ${OUT}
    done
    # SEPARATOR BETWEEN cold and warm tests
    # echo ",,,,,,," | tee -a ${OUT}
  done
done
drop_caches

# ./fa -b ${BLOCK} --${TEST} -f ${ROOT}${FILE} ${ACCESS} ${CREATE} ${SIZE} -t $t --silent
