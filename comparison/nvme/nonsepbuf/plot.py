import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import sys
import csv

reps = 5 #number of repeticiones de cada experimento


def dibujar(nombres, val_cold_seq, val_warm_seq, val_cold_rnd, val_warm_rnd):


    val_cold_seq_means = []
    val_warm_seq_means = []
    val_cold_rnd_means = []
    val_warm_rnd_means = []

    val_cold_seq_err = []
    val_warm_seq_err = []
    val_cold_rnd_err = []
    val_warm_rnd_err = []

    for list in val_cold_seq:
        val_cold_seq_means.append(np.mean(list))
        val_cold_seq_err.append(np.std(list))
    print(val_cold_seq_means)
    print(val_cold_seq_err)

    for list in val_cold_rnd:
        val_cold_rnd_means.append(np.mean(list))
        val_cold_rnd_err.append(np.std(list))
    print(val_cold_rnd_means)
    print(val_cold_rnd_err)

    for list in val_warm_seq:
        val_warm_seq_means.append(np.mean(list))
        val_warm_seq_err.append(np.std(list))
    print(val_warm_seq_means)
    print(val_warm_seq_err)

    for list in val_warm_rnd:
        val_warm_rnd_means.append(np.mean(list))
        val_warm_rnd_err.append(np.std(list))
    print(val_warm_rnd_means)
    print(val_warm_rnd_err)

    # fig, (ax1, ax2, ax3, ax4)= plt.figure()
    # ax = fig.add_subplot()  # El contenedor de los 4 subplotcitos
    # ax1 = fig.add_subplot() # cold_seq
    # ax2 = fig.add_subplot() # warm_seq
    # ax3 = fig.add_subplot() # cold_rnd
    # ax4 = fig.add_subplot() # warm_rnd



    fig, axs = plt.subplots(2,2, sharey='all')
    # Arriba seq, abajo rnd
    axs[0,0].bar(nombres, val_cold_seq_means, yerr=val_cold_seq_err, align='center', ecolor='black', capsize=8, color=['blue', 'orange', 'blue', 'orange'])
    axs[0,1].bar(nombres, val_warm_seq_means, yerr=val_warm_seq_err, align='center', ecolor='black', capsize=8, color=['blue', 'orange', 'blue', 'orange'])
    axs[1,0].bar(nombres, val_cold_rnd_means, yerr=val_cold_rnd_err, align='center', ecolor='black', capsize=8, color=['blue', 'orange', 'blue', 'orange'])
    axs[1,1].bar(nombres, val_warm_rnd_means, yerr=val_warm_rnd_err, align='center', ecolor='black', capsize=8, color=['blue', 'orange', 'blue', 'orange'])
    axs[0,0].set_title('Sequential-cold')
    axs[0,1].set_title('Sequential-warm')
    axs[1,0].set_title('Random-cold')
    axs[1,1].set_title('Random-warm')


    for ax in axs.flat:
        ax.set(ylabel='Bandwidth (GB/s)')
        # ax.label_outer()

    fig.set_size_inches([8,8])
    # fig.tight_layout(pad=0, h_pad=0, w_pad=0)


    picture=input("Insert plot name to save as EPS")
    if picture != "":
        plt.savefig(picture, format='eps')
        # plt.savefig()

    show=input("Show plot? [y/N]")
    if show == "y":
        plt.show()


def procesar(datos):
    # Bergas de los datos
    bwmean = 0.0
    tmean = 0.0
    val_cold_seq = []
    val_warm_seq = []
    val_cold_rnd = []
    val_warm_rnd = []

    bs = '8192'

    for i in range(1, len(datos), reps):
        bwmean = 0.0
        tmean = 0.0
        line = []
        if datos[i][4] != bs:
            #print("BS continue")
            continue
        # print("Outer loop ", i)
        for j in range(i, i+reps):
            line.append(float(datos[j][5]))
        # print(line)
            #print("Inner loop ", j)
            # print(data[j])

            # bwmean = bwmean + float(datos[j][5])
            # tmean = tmean + float(datos[j][6])
        # bwmean = bwmean / reps
        # tmean = tmean / reps
        # print(datos[i][1], ", ", datos[i][3],", ",datos[i][2],"mean bw: ", bwmean)
        if datos[i][1] == 'cold':
            if datos[i][2] == 'seq':
                val_cold_seq.append(line)
            elif datos[i][2] == 'rnd':
                val_cold_rnd.append(line)
        elif datos[i][1] == 'warm':
            if datos[i][2] == 'seq':
                val_warm_seq.append(line)
            elif datos[i][2] == 'rnd':
                val_warm_rnd.append(line)

    # print(val_warm_rnd)

    nombres = ('MMrd','IOrd','MMwr','IOwr')
    dibujar(nombres, val_cold_seq, val_warm_seq, val_cold_rnd, val_warm_rnd)



print("La cencia del ploteo")

while True:
    filename = input("Insert filename or press enter to exit: ")
    if filename == "":
        sys.exit("Execution terminated by user");
    try:
        with open(filename) as f:
            reader = csv.reader(f)
            data = list(reader)
            procesar(data)

    except FileNotFoundError:
        print("File not accesible")
        continue
